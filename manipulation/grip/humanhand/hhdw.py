import os
import math

import utiltools.robotmath as rm
import pandaplotutils.pandactrl as pandactrl
from panda3d.core import *
import numpy as np
from environment import collisionmodel as cm
import trimesh.primitives as tp
import copy as cp

class HHdw(object):
    """
    Human hand defined following Weiwei's hand size
    """

    def __init__(self, *args, **kwargs):
        """
        load the robotiq85 model, set jawwidth and return a nodepath
        the hhdw gripper is composed of a parallelism and a fixed triangle,
        the parallelism: 1.905-1.905; 5.715-5.715; 70/110 degree
        the triangle: 4.75 (finger) 5.715 (inner knuckle) 3.175 (outer knuckle)

        :param args:
        :param kwargs:
            'jawwidth' 0-85
            'ftsensoroffset' the offset for forcesensor
            'togglefrmes' True, False

        author: weiwei
        date: 20160627, 20190518
        """

        palmwidth = 85
        palmheight = 120
        palmthick = 35

        thumbprox = 40
        thumbdist = 25
        thumbthick = 18
        thumbwidth = 23

        indexprox = 50
        indexmiddle = 25
        indexdist = 25
        indexthick = 17
        indexwidth = 17

        middleprox = 55
        middlemiddle = 35
        middledist = 25
        middlethick = 16
        middlewidth = 16

        ringprox = 50
        ringmiddle = 30
        ringdist = 23
        ringthick = 13
        ringwidth = 15

        littleprox = 40
        littlemiddle = 25
        littledist = 23
        littlethick = 12
        littlewidth = 15

        if 'jawwidth' in kwargs:
            self.__jawwidth = kwargs['jawwidth']
        if 'ftsensoroffset' in kwargs:
            self.__ftsensoroffset = kwargs['ftsensoroffset']
        if 'toggleframes' in kwargs:
            self.__toggleframes = kwargs['toggleframes']
        if 'armname' in kwargs:
            self.__armname = kwargs['armname']
        else:
            self.__armname = 'rgt'

        self.__name = "hhdw"
        self.__hhdwnp = NodePath("hhdwhnd")
        self.__eetip = np.array([0.0, 0.0, 145.0])+np.array([0.0, 0.0, self.__ftsensoroffset])
        self.__jawwidthopen = 85.0
        self.__jawwidthclosed = 0.0

        # base
        self.__hhdwbase = cm.CollisionModel(tp.Box(box_center = [0,0,palmheight/2],
                                                   box_extents=[palmthick, palmwidth, palmheight]))
        self.__hhdwbase.setRPY(0,-25,0)
        self.__hhdwbase.setPos(0.0, 0.0, self.__ftsensoroffset)

        # ftsensor
        if self.__ftsensoroffset > 0:
            self.__ftsensor = cm.CollisionModel(tp.Cylinder(height=self.__ftsensoroffset, radius=30), name="ftsensor")
            self.__ftsensor.setPos(0,0,-self.__ftsensoroffset/2)
            # ftsensor.setRPY(90,0,0)
            self.__ftsensor.reparentTo(self.__hhdwbase)
        else:
            self.__ftsensor = None

        # basehomo = self.__hhdwbase.gethomomat4()
        # localy = basehomo[:3,1]
        # localz = basehomo[:3,2]

        fgrdist = 5

        # thumb
        self.__hhdwthumbprox = cm.CollisionModel(tp.Box(box_center = [0,0,thumbprox/2],
                                                    box_extents=[thumbthick, thumbwidth, thumbprox]))
        self.__hhdwthumbprox.reparentTo(self.__hhdwbase)
        self.__hhdwthumbprox.setPos(thumbthick, palmwidth/2-thumbwidth/2, palmheight-50)
        if self.__armname == 'lft':
            self.__hhdwthumbprox.setPos(thumbthick, -palmwidth/2+thumbwidth/2, palmheight-50)
        self.__hhdwthumbprox.setRPY(0,60,0)
        self.__hhdwthumbdist = cm.CollisionModel(tp.Box(box_center = [0,0,thumbdist/2],
                                                    box_extents=[thumbthick, thumbwidth, thumbdist]))
        self.__hhdwthumbdist.reparentTo(self.__hhdwthumbprox)
        self.__hhdwthumbdist.setPos(0, 0, thumbprox)
        self.__hhdwthumbdist.setRPY(0,-15,0)

        # index
        self.__hhdwindexprox = cm.CollisionModel(tp.Box(box_center = [0,0,indexprox/2],
                                                    box_extents=[indexthick, indexwidth, indexprox]))
        self.__hhdwindexprox.reparentTo(self.__hhdwbase)
        self.__hhdwindexprox.setRPY(0,25,0)
        # pos = localy*(palmwidth/2)-localy*(indexwidth/2)+localz*palmheight
        # self.__hhdwindexprox.setPos(pos[0], pos[1], pos[2])
        self.__hhdwindexprox.setPos(0, palmwidth/2-indexwidth/2-fgrdist, palmheight)
        if self.__armname == 'lft':
            self.__hhdwindexprox.setPos(0, -palmwidth/2+indexwidth/2+fgrdist, palmheight)
        self.__hhdwindexmiddle = cm.CollisionModel(tp.Box(box_center = [0,0,indexmiddle/2],
                                                    box_extents=[indexthick, indexwidth, indexmiddle]))
        self.__hhdwindexmiddle.reparentTo(self.__hhdwindexprox)
        self.__hhdwindexmiddle.setPos(0, 0, indexprox)
        self.__hhdwindexmiddle.setRPY(0,15,0)
        self.__hhdwindexdist = cm.CollisionModel(tp.Box(box_center = [0,0,indexdist/2],
                                                    box_extents=[indexthick, indexwidth, indexdist]))
        self.__hhdwindexdist.reparentTo(self.__hhdwindexmiddle)
        self.__hhdwindexdist.setPos(0, 0, indexmiddle)
        self.__hhdwindexdist.setRPY(0,5,0)

        # middle
        self.__hhdwmiddleprox = cm.CollisionModel(tp.Box(box_center = [0,0,middleprox/2],
                                                    box_extents=[middlethick, middlewidth, middleprox]))
        self.__hhdwmiddleprox.reparentTo(self.__hhdwbase)
        self.__hhdwmiddleprox.setRPY(0,22,0)
        # pos = localy*(palmwidth/2)-localy*(indexwidth+middlewidth/2)+localz*palmheight
        # self.__hhdwmiddleprox.setPos(pos[0], pos[1], pos[2])
        self.__hhdwmiddleprox.setPos(0, palmwidth/2-indexwidth-middlewidth/2-2*fgrdist, palmheight)
        if self.__armname == 'lft':
            self.__hhdwmiddleprox.setPos(0, -palmwidth/2+indexwidth+middlewidth/2+2*fgrdist, palmheight)
        self.__hhdwmiddlemiddle = cm.CollisionModel(tp.Box(box_center = [0,0,middlemiddle/2],
                                                    box_extents=[middlethick, middlewidth, middlemiddle]))
        self.__hhdwmiddlemiddle.reparentTo(self.__hhdwmiddleprox)
        self.__hhdwmiddlemiddle.setPos(0, 0, middleprox)
        self.__hhdwmiddlemiddle.setRPY(0,16,0)
        self.__hhdwmiddledist = cm.CollisionModel(tp.Box(box_center = [0,0,middledist/2],
                                                    box_extents=[middlethick, middlewidth, middledist]))
        self.__hhdwmiddledist.reparentTo(self.__hhdwmiddlemiddle)
        self.__hhdwmiddledist.setPos(0, 0, middlemiddle)
        self.__hhdwmiddledist.setRPY(0,7,0)

        # ring
        self.__hhdwringprox = cm.CollisionModel(tp.Box(box_center = [0,0,ringprox/2],
                                                    box_extents=[ringthick, ringwidth, ringprox]))
        self.__hhdwringprox.reparentTo(self.__hhdwbase)
        self.__hhdwringprox.setRPY(0,27,0)
        # pos = localy*(palmwidth/2)-localy*(indexwidth+middlewidth+ringwidth/2)+localz*palmheight
        # self.__hhdwringprox.setPos(pos[0], pos[1], pos[2])
        self.__hhdwringprox.setPos(0, palmwidth/2-indexwidth-middlewidth-ringwidth/2-3*fgrdist, palmheight)
        if self.__armname == 'lft':
            self.__hhdwringprox.setPos(0, -palmwidth/2+indexwidth+middlewidth+ringwidth/2+3*fgrdist, palmheight)
        self.__hhdwringmiddle = cm.CollisionModel(tp.Box(box_center = [0,0,ringmiddle/2],
                                                    box_extents=[ringthick, ringwidth, ringmiddle]))
        self.__hhdwringmiddle.reparentTo(self.__hhdwringprox)
        self.__hhdwringmiddle.setPos(0, 0, ringprox)
        self.__hhdwringmiddle.setRPY(0,13,0)
        self.__hhdwringdist = cm.CollisionModel(tp.Box(box_center = [0,0,ringdist/2],
                                                    box_extents=[ringthick, ringwidth, ringdist]))
        self.__hhdwringdist.reparentTo(self.__hhdwringmiddle)
        self.__hhdwringdist.setPos(0, 0, ringmiddle)
        self.__hhdwringdist.setRPY(0,5,0)

        # little
        self.__hhdwlittleprox = cm.CollisionModel(tp.Box(box_center = [0,0,littleprox/2],
                                                    box_extents=[littlethick, littlewidth, littleprox]))
        self.__hhdwlittleprox.reparentTo(self.__hhdwbase)
        self.__hhdwlittleprox.setRPY(0,30,0)
        # pos = localy*(palmwidth/2)-localy*(indexwidth+middlewidth+ringwidth+littlewidth/2)+localz*palmheight
        # self.__hhdwlittleprox.setPos(pos[0], pos[1], pos[2])
        self.__hhdwlittleprox.setPos(0, palmwidth/2-indexwidth-middlewidth-ringwidth-littlewidth/2-4*fgrdist, palmheight)
        if self.__armname == 'lft':
            self.__hhdwlittleprox.setPos(0, -palmwidth/2+indexwidth+middlewidth+ringwidth/2+littlewidth/2+4*fgrdist, palmheight)
        self.__hhdwlittlemiddle = cm.CollisionModel(tp.Box(box_center = [0,0,littlemiddle/2],
                                                    box_extents=[littlethick, littlewidth, littlemiddle]))
        self.__hhdwlittlemiddle.reparentTo(self.__hhdwlittleprox)
        self.__hhdwlittlemiddle.setPos(0, 0, littleprox)
        self.__hhdwlittlemiddle.setRPY(0,15,0)
        self.__hhdwlittledist = cm.CollisionModel(tp.Box(box_center = [0,0,littledist/2],
                                                    box_extents=[littlethick, littlewidth, littledist]))
        self.__hhdwlittledist.reparentTo(self.__hhdwlittlemiddle)
        self.__hhdwlittledist.setPos(0, 0, littlemiddle)

        # rotate to x, y, z coordinates (this one rotates the base, not the self.hhdwnp)
        # the default x direction is facing the ee, the default z direction is facing downward
        # execute this file to see the default pose
        self.__hhdwbase.reparentTo(self.__hhdwnp)

        self.setJawwidth(self.__jawwidth)

        # default color
        self.setDefaultColor()

        if self.__toggleframes:
            if self.__ftsensor is not None:
                self.__ftsensor = base.pggen.genAxis()
                self.__ftsensor.reparentTo(self.__hhdwnp)
            self.__hhdwhndframe = base.pggen.genAxis()
            self.__hhdwhndframe.reparentTo(self.__hhdwnp)
            self.__hhdwbaseframe = base.pggen.genAxis()
            self.__hhdwbaseframe.reparentTo(self.__hhdwbase.objnp)
            # self.__hhdwilknuckleframe = base.pggen.genAxis()
            # self.__hhdwilknuckleframe.reparentTo(self.__hhdwilknuckle.objnp)
            # self.__hhdwlknuckleframe = base.pggen.genAxis()
            # self.__hhdwlknuckleframe.reparentTo(self.__hhdwlknuckle.objnp)
            # self.__hhdwlfgrframe = base.pggen.genAxis()
            # self.__hhdwlfgrframe.reparentTo(self.__hhdwlfgr.objnp)
            # self.__hhdwlfgrtipframe = base.pggen.genAxis()
            # self.__hhdwlfgrtipframe.reparentTo(self.__hhdwlfgrtip.objnp)
            # self.__hhdwirknuckleframe = base.pggen.genAxis()
            # self.__hhdwirknuckleframe.reparentTo(self.__hhdwirknuckle.objnp)
            # self.__hhdwrknuckleframe = base.pggen.genAxis()
            # self.__hhdwrknuckleframe.reparentTo(self.__hhdwrknuckle.objnp)
            # self.__hhdwrfgrframe = base.pggen.genAxis()
            # self.__hhdwrfgrframe.reparentTo(self.__hhdwrfgr.objnp)
            # self.__hhdwrfgrtipframe = base.pggen.genAxis()
            # self.__hhdwrfgrtipframe.reparentTo(self.__hhdwrfgrtip.objnp)

    @property
    def handnp(self):
        # read-only property
        return self.__hhdwnp

    @property
    def jawwidthopen(self):
        # read-only property
        return self.__jawwidthopen

    @property
    def jawwidthclosed(self):
        # read-only property
        return self.__jawwidthclosed

    @property
    def jawwidth(self):
        # read-only property
        return self.__jawwidth

    @property
    def cmlist(self):
        # read-only property
        if self.__ftsensor is not None:
            return [self.__ftsensor, self.__hhdwbase, self.__hhdwthumbprox, self.__hhdwthumbdist,
                    self.__hhdwindexprox, self.__hhdwindexmiddle, self.__hhdwindexdist,
                    self.__hhdwmiddleprox, self.__hhdwmiddlemiddle, self.__hhdwmiddledist,
                    self.__hhdwringprox, self.__hhdwringmiddle, self.__hhdwringdist,
                    self.__hhdwlittleprox, self.__hhdwlittlemiddle, self.__hhdwlittledist]
        else:
            return [self.__hhdwbase, self.__hhdwthumbprox, self.__hhdwthumbdist,
                    self.__hhdwindexprox, self.__hhdwindexmiddle, self.__hhdwindexdist,
                    self.__hhdwmiddleprox, self.__hhdwmiddlemiddle, self.__hhdwmiddledist,
                    self.__hhdwringprox, self.__hhdwringmiddle, self.__hhdwringdist,
                    self.__hhdwlittleprox, self.__hhdwlittlemiddle, self.__hhdwlittledist]

    @property
    def eetip(self):
        # read-only property
        return self.__eetip

    @property
    def name(self):
        # read-only property
        return self.__name

    def setJawwidth(self, jawwidth):
        """
        set the jawwidth of hhdwhnd
        the hhdw gripper is composed of a parallelism and a fixed triangle,
        the parallelism: 1.905-1.905; 5.715-5.715; 70/110 degree
        the triangle: 4.75 (finger) 5.715 (inner knuckle) 3.175 (outer knuckle)

        :param jawwidth: mm
        :return:

        author: weiwei
        date: 20160627, 20190514
        """

        if jawwidth > 85.0 or jawwidth < 0.0:
            print ("Wrong value! Jawwidth must be in (0.0,85.0). The input is "+str(jawwidth)+".")
            raise Exception("Jawwidth out of range!")

        rotiknuckle=math.degrees(math.asin((jawwidth/2-5)/57.15))

        # # right finger
        # hhdwirknucklerpy = self.__hhdwirknuckle.getRPY()
        # self.__hhdwirknuckle.setRPY(hhdwirknucklerpy[0], rotiknuckle, hhdwirknucklerpy[2])
        # hhdwrknucklerpy = self.__hhdwrknuckle.getRPY()
        # self.__hhdwrknuckle.setRPY(hhdwrknucklerpy[0], rotiknuckle-33.78, hhdwrknucklerpy[2])
        # hhdwrfgrtiprpy = self.__hhdwrfgrtip.getRPY()
        # self.__hhdwrfgrtip.setRPY(hhdwrfgrtiprpy[0], -rotiknuckle, hhdwrfgrtiprpy[2])
        #
        # # left finger
        # hhdwilknucklerpy = self.__hhdwilknuckle.getRPY()
        # self.__hhdwilknuckle.setRPY(hhdwilknucklerpy[0], rotiknuckle, hhdwilknucklerpy[2])
        # hhdwlknucklerpy = self.__hhdwlknuckle.getRPY()
        # self.__hhdwlknuckle.setRPY(hhdwlknucklerpy[0], rotiknuckle-33.78, hhdwlknucklerpy[2])
        # hhdwlfgrtiprpy = self.__hhdwlfgrtip.getRPY()
        # self.__hhdwlfgrtip.setRPY(hhdwlfgrtiprpy[0], -rotiknuckle, hhdwlfgrtiprpy[2])

        # update the private member variable
        self.__jawwidth = jawwidth

    def setPos(self, pandavec3):
        """
        set the pose of the hand
        changes self.hhdwnp

        :param pandavec3 panda3d vec3
        :return:
        """

        self.__hhdwnp.setPos(pandavec3)

    def getPos(self):
        """
        set the pose of the hand
        changes self.hhdwnp

        :param pandavec3 panda3d vec3
        :return:
        """

        return self.__hhdwnp.getPos()

    def setMat(self, pandamat4):
        """
        set the translation and rotation of a robotiq hand
        changes self.hhdwnp

        :param pandamat4 panda3d Mat4
        :return: null

        date: 20161109
        author: weiwei
        """

        self.__hhdwnp.setMat(pandamat4)

    def getMat(self):
        """
        get the rotation matrix of the hand

        :return: pandamat4 follows panda3d, a LMatrix4f matrix

        date: 20161109
        author: weiwei
        """

        return self.__hhdwnp.getMat()

    def reparentTo(self, nodepath):
        """
        add to scene, follows panda3d

        :param nodepath: a panda3d nodepath
        :return: null

        date: 20161109
        author: weiwei
        """
        self.__hhdwnp.reparentTo(nodepath)

    def removeNode(self):
        """

        :return:
        """

        self.__hhdwnp.removeNode()

    def __lookAt(self, direct0, direct1, direct2):
        """
        set the Y axis of the hnd
        ** deprecated 20190517

        author: weiwei
        date: 20161212
        """

        self.__hhdwnp.lookAt(direct0, direct1, direct2)

    def gripAt(self, fcx, fcy, fcz, c0nx, c0ny, c0nz, rotangle = 0, jawwidth = 82):
        """
        set the hand to grip at fcx, fcy, fcz, fc = finger center
        the normal of the sglfgr contact is set to be c0nx, c0ny, c0nz
        the rotation around the normal is set to rotangle
        the jawwidth is set to jawwidth

        date: 20170322
        author: weiwei
        """

        # x is the opening direction of the hand, _org means the value when rotangle=0
        standardx_org = np.array([1,0,0])
        newx_org = np.array([c0nx, c0ny, c0nz])
        rotangle_org = rm.degree_betweenvector(newx_org, standardx_org)
        rotaxis_org = rm.unitvec_safe(np.cross(standardx_org, newx_org))[1]
        newrotmat_org = rm.rodrigues(rotaxis_org, rotangle_org)

        # rotate to the given rotangle
        hnd_rotmat4 = np.eye(4)
        hnd_rotmat4[:3,:3] = np.dot(rm.rodrigues(newx_org, rotangle), newrotmat_org)
        handtipnpvec3 = np.array([fcx, fcy, fcz])-np.dot(hnd_rotmat4[:3,:3], self.__eetip)
        hnd_rotmat4[:3,3] = handtipnpvec3
        self.__hhdwnp.setMat(base.pg.np4ToMat4(hnd_rotmat4))
        self.setJawwidth(jawwidth)

        return [jawwidth, np.array([fcx, fcy, fcz]), hnd_rotmat4]

    def approachAt(self, fcx, fcy, fcz, c0nx, c0ny, c0nz, apx, apy, apz, jawwidth = 82):
        """
        set the hand to grip at fcx, fcy, fcz, fc = finger center
        the normal of the sglfgr contact is set to be c0nx, c0ny, c0nz
        the approach vector of the hand is set to apx, apy, apz
        the jawwidth is set to jawwidth

        date: 20190528
        author: weiwei
        """

        # x is the opening direction of the hand, _org means the value when rotangle=0
        nphndmat3 = np.eye(3)
        nphndmat3[:3,0] = rm.unitvec_safe(np.array([c0nx, c0ny, c0nz]))[1]
        nphndmat3[:3,2] = rm.unitvec_safe(np.array([apx, apy, apz]))[1]
        nphndmat3[:3,1] = rm.unitvec_safe(np.cross(np.array([apx, apy, apz]), np.array([c0nx, c0ny, c0nz])))[1]

        # rotate to the given rotangle
        hnd_rotmat4 = np.eye(4)
        hnd_rotmat4[:3,:3] = nphndmat3
        handtipnpvec3 = np.array([fcx, fcy, fcz])-np.dot(nphndmat3, self.__eetip)
        hnd_rotmat4[:3,3] = handtipnpvec3
        self.__hhdwnp.setMat(base.pg.np4ToMat4(hnd_rotmat4))
        self.setJawwidth(jawwidth)

        return [jawwidth, np.array([fcx, fcy, fcz]), hnd_rotmat4]

    def setColor(self, *args, **kwargs):
        """

        :param rgba
        :return:

        author: weiwei
        date: 20190514
        """

        if self.__ftsensor is not None:
            self.__ftsensor.clearColor()
        self.__hhdwbase.clearColor()
        self.__hhdwthumbprox.clearColor()
        self.__hhdwthumbdist.clearColor()
        self.__hhdwindexprox.clearColor()
        self.__hhdwindexmiddle.clearColor()
        self.__hhdwindexdist.clearColor()
        self.__hhdwmiddleprox.clearColor()
        self.__hhdwmiddlemiddle.clearColor()
        self.__hhdwmiddledist.clearColor()
        self.__hhdwringprox.clearColor()
        self.__hhdwringmiddle.clearColor()
        self.__hhdwringdist.clearColor()
        self.__hhdwlittleprox.clearColor()
        self.__hhdwlittlemiddle.clearColor()
        self.__hhdwlittledist.clearColor()
        if len(args)==1:
            self.__hhdwnp.setColor(args[0])
        else:
            # print(args[0], args[1], args[2], args[3])
            self.__hhdwnp.setColor(args[0], args[1], args[2], args[3])

    def setDefaultColor(self, rgba_finger=[.8, .6, .3, .5], rgba_base=[.8, .6, .3, .5]):
        """

        :param rbga_finger:
        :param rgba_base:
        :return:

        author: weiwei
        date: 20190514
        """

        if self.__ftsensor is not None:
            self.__ftsensor.setColor(rgba_finger[0], rgba_finger[1], rgba_finger[2], rgba_finger[3])
        self.__hhdwbase.setColor(rgba_finger[0], rgba_finger[1], rgba_finger[2], rgba_finger[3])
        self.__hhdwthumbprox.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwthumbdist.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwindexprox.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwindexmiddle.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwindexdist.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwmiddleprox.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwmiddlemiddle.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwmiddledist.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwringprox.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwringmiddle.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwringdist.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwlittleprox.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwlittlemiddle.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])
        self.__hhdwlittledist.setColor(rgba_base[0], rgba_base[1], rgba_base[2], rgba_base[3])

    def copy(self):
        """
        make a copy of oneself
        use this one to copy a hand since copy.deepcopy doesnt work well with the collisionnode

        :return:

        author: weiwei
        date: 20190525osaka
        """

        hand = HHdw(jawwidth=self.__jawwidth, ftsensoroffset=self.__ftsensoroffset, toggleframes=self.__toggleframes,
                    armname=self.__armname)
        # hand.setColor(self.__hhdwnp.getColor())
        return hand

    def showcn(self):
        self.__hhdwbase.showcn()
        self.__hhdwthumbprox.showcn()
        self.__hhdwthumbdist.showcn()
        self.__hhdwindexprox.showcn()
        self.__hhdwindexmiddle.showcn()
        self.__hhdwindexdist.showcn()
        self.__hhdwmiddleprox.showcn()
        self.__hhdwmiddlemiddle.showcn()
        self.__hhdwmiddledist.showcn()
        self.__hhdwringprox.showcn()
        self.__hhdwringmiddle.showcn()
        self.__hhdwringdist.showcn()
        self.__hhdwlittleprox.showcn()
        self.__hhdwlittlemiddle.showcn()
        self.__hhdwlittledist.showcn()

class HHdwFactory(object):

    def __init__(self):
        this_dir, this_filename = os.path.split(__file__)

    def genHand(self, jawwidth=85, ftsensoroffset = 52, toggleframes=False, armname="rgt"):
        return HHdw(jawwidth=jawwidth, ftsensoroffset=ftsensoroffset, toggleframes=toggleframes, armname=armname)

if __name__=='__main__':
    import copy as cp
    import environment.bulletcdhelper as bcd

    base = pandactrl.World(lookatp=[0,0,0])
    base.pggen.plotAxis(base.render)

    rfa = HHdwFactory()
    hhdwhnd = rfa.genHand(ftsensoroffset=0, armname='lft')
    # hhdwhnd.gripAt(100,0,0,0,1,0,30,5)
    hhdwhnd.reparentTo(base.render)
    # hhdwhnd.showcn()

    # hhdwhnd2 = rfa.genHand()
    # hhdwhnd2.gripAt(-40, -18, 33, -0.9, 0.4, 0.0,60,13.204926835369958)
    # # hhdwhnd2.setColor(1,0,0,.2)
    # hhdwhnd2.reparentTo(base.render)
    # hhdwhnd3 = hhdwhnd2.copy()
    # hhdwhnd3.gripAt(0,0,0, 0,1,0,90,45)
    # hhdwhnd3.reparentTo(base.render)
    # hhdwhnd2.showcn()

    # bmc = bcd.MCMchecker(toggledebug=True)
    # print(bmc.isMeshListMeshListCollided(hhdwhnd.cmlist, hhdwhnd2.cmlist))
    # bmc.showMeshList(hhdwhnd.cmlist)
    # bmc.showMeshList(hhdwhnd2.cmlist)
    # bmc.showMeshList(hhdwhnd3.cmlist)
    # bmc.showBoxList(hhdwhnd.cmlist)
    # bmc.showBoxList(hhdwhnd2.handnp)
    base.run()