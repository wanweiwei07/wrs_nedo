import utiltools.robotmath as rm
import numpy as np
import copy
import motionplanning.collisioncheckerball as cdball
import environment.bulletcdhelper as bch

# def getAvailableGrasps(idlist, predefined_graspss, objpos, objrot, obscmlist, rbt, rbtball):

def getsymmetricgoals(finalnpmat4):
    """
    find symmetric goals that are essential the same pose
    only applicable to the domino.stl object

    :param finalnpmat4:
    :return:
    """

    objmat4_final = finalnpmat4
    objpos_final = objmat4_final[:3, 3]
    objrot_final = objmat4_final[:3, :3]

    objpos_final0 = copy.deepcopy(objpos_final)
    objrot_final0 = objrot_final
    objpos_final1 = copy.deepcopy(objpos_final)
    objrot_final1 = np.dot(rm.rodrigues(objmat4_final[:3, 2], 180), objrot_final)
    objpos_final2 = copy.deepcopy(objpos_final)
    objrot_final2 = np.dot(rm.rodrigues(objmat4_final[:3, 0], 180), objrot_final)
    objpos_final2 -= 120 * objrot_final2[:3, 2]
    objpos_final3 = copy.deepcopy(objpos_final)
    objrot_final3 = np.dot(rm.rodrigues(objmat4_final[:3, 0], 180), objrot_final)
    objrot_final3 = np.dot(rm.rodrigues(objmat4_final[:3, 2], 180), objrot_final3)
    objpos_final3 -= 120 * objrot_final3[:3, 2]

    objpos_finallist = [objpos_final0, objpos_final1, objpos_final2, objpos_final3]
    objrot_finallist = [objrot_final0, objrot_final1, objrot_final2, objrot_final3]

    return [objpos_finallist, objrot_finallist]

def findsharedgrasps(initpos, initrot, goalpos, goalrot, predefined_graspss, obj, rbt, rbtmg, rbtball, armname, hndfa, obscmlist):
    """

    :param initpos:
    :param initrot:
    :param goalpos:
    :param goalrot:
    :param predefined_graspss:
    :param obj:
    :param rbt:
    :param rbtball:
    :param rbtmg:
    :param armname:
    :param hndfa:
    :param obscmlist:
    :return:
    """

    toggledebug = False

    pcdchecker = cdball.CollisionCheckerBall(rbtball)
    bcdchecker = bch.MCMchecker(toggledebug=False)

    # start pose
    objpos = initpos
    objrot = initrot
    objmat4=rm.homobuild(objpos, objrot)
    obj.setMat(base.pg.np4ToMat4(objmat4))

    availablePre = []
    for idpre, predefined_grasp in enumerate(predefined_graspss):
        prejawwidth, prehndfc, prehndmat4 = predefined_grasp
        hndmat4 = np.dot(objmat4, prehndmat4)
        eepos = rm.homotransform(objmat4, prehndfc)[:3]
        eerot = hndmat4[:3,:3]

        hndnew = hndfa.genHand()
        hndnew.setMat(base.pg.np4ToMat4(hndmat4))
        hndnew.setJawwidth(prejawwidth)
        isHndCollided = bcdchecker.isMeshListMeshListCollided(hndnew.cmlist, obscmlist)
        if not isHndCollided:
            armjnts = rbt.numik(eepos, eerot, armname)
            if armjnts is not None:
                hndnew = hndfa.genHand()
                hndnew.setColor(0, 1, 0, .5)
                hndnew.setMat(base.pg.np4ToMat4(hndmat4))
                hndnew.setJawwidth(prejawwidth)
                if toggledebug:
                    hndnew.reparentTo(base.render)
                rbt.movearmfk(armjnts, armname)
                isRbtCollided = pcdchecker.isRobotCollided(rbt, obscmlist, holdarmname=armname)
                # isObjCollided = pcdchecker.isObjectsOthersCollided([obj], rbt, armname, obscmlist)
                isObjCollided = False
                # print(isRbtCollided, isObjCollided)
                # print(isRbtCollided,isObjCollided)
                if (not isRbtCollided) and (not isObjCollided):
                    # rbtball.showcn(rbtball.genholdbcndict(rbt, armname))
                    if toggledebug:
                        rbtmg.genmnp(rbt, jawwidthrgt=prejawwidth, drawhand=False, togglejntscoord=False, rgbargt=[0, 1, 0, .5]).reparentTo(base.render)
                    availablePre.append(idpre)
                elif (not isObjCollided):
                    if toggledebug:
                        rbtmg.genmnp(rbt, jawwidthrgt=prejawwidth, drawhand=False, togglejntscoord=False, rgbargt=[1, 0, 1, .5]).reparentTo(base.render)
                    pass
                else:
                    pass
            else:
                hndnew = hndfa.genHand()
                hndnew.setColor(1, .6, 0, .5)
                hndnew.setMat(base.pg.np4ToMat4(hndmat4))
                hndnew.setJawwidth(prejawwidth)
                if toggledebug:
                    hndnew.reparentTo(base.render)
                # bcdchecker.showMeshList(hndnew.cmlist)

                pass
        else:
            hndnew = hndfa.genHand()
            hndnew.setColor(1, 0, 1, .5)
            hndnew.setMat(base.pg.np4ToMat4(hndmat4))
            hndnew.setJawwidth(prejawwidth)
            if toggledebug:
                hndnew.reparentTo(base.render)
            # bcdchecker.showMeshList(hndnew.cmlist)
            pass

    # goal pose
    objpos = goalpos
    objrot = goalrot
    objmat4=rm.homobuild(objpos, objrot)
    obj.setMat(base.pg.np4ToMat4(objmat4))

    finalResult = []
    print(availablePre)
    for idpre in availablePre:
        prejawwidth, prehndfc, prehndmat4 = predefined_graspss[idpre]
        hndmat4 = np.dot(objmat4, prehndmat4)
        eepos = rm.homotransform(objmat4, prehndfc)[:3]
        eerot = hndmat4[:3,:3]
        # print(eepos, eerot)
        hndnew = hndfa.genHand()
        hndnew.setMat(base.pg.np4ToMat4(hndmat4))
        hndnew.setJawwidth(prejawwidth)
        isHndCollided = bcdchecker.isMeshListMeshListCollided(hndnew.cmlist, obscmlist)
        # print(isHndCollided)
        if not isHndCollided:
            armjnts = rbt.numik(eepos, eerot, armname)
            if armjnts is not None:
                hndnew = hndfa.genHand()
                hndnew.setColor(0, 1, 0, .5)
                hndnew.setMat(base.pg.np4ToMat4(hndmat4))
                hndnew.setJawwidth(prejawwidth)
                if toggledebug:
                    hndnew.reparentTo(base.render)
                # bcdchecker.showMeshList(hndnew.cmlist)
                rbt.movearmfk(armjnts, armname)
                isRbtCollided = pcdchecker.isRobotCollided(rbt, obscmlist, holdarmname=armname)
                # isObjCollided = pcdchecker.isObjectsOthersCollided([obj], rbt, armname, obscmlist)
                isObjCollided = False
                # print(isRbtCollided, isObjCollided)
                if (not isRbtCollided) and (not isObjCollided):
                    if toggledebug:
                        rbtmg.genmnp(rbt, jawwidthrgt=prejawwidth, togglejntscoord=False, rgbalft=[0, 1, 0, .5]).reparentTo(base.render)
                    # print(idpre)
                    finalResult.append(idpre)
                    pass
                elif (not isObjCollided):
                    if toggledebug:
                        rbtmg.genmnp(rbt, jawwidthrgt=prejawwidth, drawhand=False, togglejntscoord=False, rgbalft=[1, 0, 1, .5]).reparentTo(base.render)
                    pass
                else:
                    pass
            else:
                hndnew = hndfa.genHand()
                hndnew.setColor(1, .6, 0, .5)
                hndnew.setMat(base.pg.np4ToMat4(hndmat4))
                hndnew.setJawwidth(prejawwidth)
                if toggledebug:
                    hndnew.reparentTo(base.render)
                # bcdchecker.showMeshList(hndnew.cmlist)
        else:
            hndnew = hndfa.genHand()
            hndnew.setColor(1, 0, 1, .5)
            hndnew.setMat(base.pg.np4ToMat4(hndmat4))
            hndnew.setJawwidth(prejawwidth)
            if toggledebug:
                hndnew.reparentTo(base.render)
            # bcdchecker.showMeshList(hndnew.cmlist)
    # base.run()
    return finalResult