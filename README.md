# The WRS code

## Structure of the package

1. The 0000_example folder includes a bunch of examples showing the usages of the libraries.
2. The remaining folders are library folders
3. database
    * Code related to the relational database (RDB)
4. environment
    * Code related to the definition of the environment
    * A collision model file is implemented to define an object/obstacle
5. manipulation
    * Code related to Grasps, Regrasps, Assembly, etc.
6. motion planning
    * Code related to motion planning algorithms
7. pandaplotutils
    * Defines the fundamental settings of the panda3d rendering environment, and the utitily functions that
    convert datatypes between numpy and panda3d
8. robotcon
    * Code related to controlling real-world robots
9. robotsim
    * Code related to simulation
    * The robot kinematic structures are defined here
10. trimesh
    * Inheritted from Miked
11. utils
    * General python utility functions
12. vision
    * Code related to cameras, kinects, and other external devices
    * The RPYC servers are implemented here
    
## Reference

- Planning Grasps for Assembly Tasks, Weiwei Wan, Kensuke Harada, and Fumio Kanehiro, arXiv: 1903.01631 [cs.RO], 2019.
- Dual-arm Assembly Planning Considering Gravitational Constraints, Ryota Moriyama, Weiwei Wan, and Kensuke Harada, IEEE International Conference on Intelligent Robots and Systems (IROS), 2019.
- Preparatory Manipulation Planning using Automatically Determined Single and Dual Arms, Weiwei Wan, Kensuke Harada, and Fumio Kanehiro, IEEE Transactions on Industrial Informatics (TII), 2019.
- Developing and Comparing Single-arm and Dual-arm Regrasp, Weiwei Wan and Kensuke Harada. IEEE Robotics and Automation Letters (RAL), 2016. 


 
## Go to the README in the example folder for details!
