from pandaplotutils import pandactrl as pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import robotsim.ur3dual.ur3dual as robot
import robotsim.ur3dual.ur3dualmesh as robotmesh
import robotsim.ur3dual.ur3dualball as robotball
import utiltools.robotmath as rm
import numpy as np
import environment.collisionmodel as cm
import bunrisettingfree as bsf
import motionplanning.collisioncheckerball as cdball
import environment.bulletcdhelper as bch
from motionplanning import collisioncheckerball as cdck
from motionplanning import ctcallback as ctcb
from motionplanning.rrt import rrtconnect as rrtc
from motionplanning import smoother as sm
import copy
import math

# def getAvailableGrasps(idlist, predefined_graspss, objpos, objrot, obscmlist, rbt, rbtball):


if __name__=="__main__":

    base = pandactrl.World(camp = [5000,3000,3000], lookatp = [0,0,700])

    env = bsf.Env(betransparent=True)
    obscmlist = env.getstationaryobslist()
    # for obscm in obscmlist:
    #     obscm.setColor(.3,.3,.3,.5)
    env.reparentTo(base.render)
    obscmlist = env.getstationaryobslist()

    hndfa = rtq85.Robotiq85Factory()
    rtq85 = hndfa.genHand()
    base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
    predefined_grasps = []
    predefined_grasps.append(rtq85.approachAt(0,0,90,0,1,0,-.707,0,-.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,90,0,1,0,.707,0,-.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,90,0,1,0,-.707,0,.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,90,0,1,0,.707,0,.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,90,0,1,0,-1,0,0,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,90,0,1,0,1,0,0,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,30,0,1,0,-.707,0,.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,30,0,1,0,.707,0,.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,30,0,1,0,-.707,0,-.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,30,0,1,0,.707,0,-.707,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,30,0,1,0,-1,0,0,jawwidth=60))
    predefined_grasps.append(rtq85.approachAt(0,0,30,0,1,0,1,0,0,jawwidth=60))

    armname="rgt"

    lfthnd = hndfa.genHand()
    rgthnd = hndfa.genHand()
    rbt = robot.Ur3DualRobot(rgthnd, lfthnd)
    rbt.goinitpose()
    rbtmg = robotmesh.Ur3DualMesh()
    # rbtmg.genmnp(rbt, togglejntscoord=False).reparentTo(base.render)
    rbtball = robotball.Ur3DualBall()
    pcdchecker = cdball.CollisionCheckerBall(rbtball)
    bcdchecker = bch.MCMchecker(toggledebug=False)
    bcdchecker.showMeshList(obscmlist)

    # load object
    obj = cm.CollisionModel(objinit="./objects/domino.stl")

    # draw the object at the initial object pose
    objpos_initial = np.array([450,-100,1050])
    objrot_initial = np.dot(rm.rodrigues([0,0,1],-30), rm.rodrigues([0,1,0],-90))
    objmat4_initial = rm.homobuild(objpos_initial, objrot_initial)
    obj_initial = copy.deepcopy(obj)
    obj_initial.setColor(1, 0, 0, .5)
    obj_initial.setMat(base.pg.np4ToMat4(objmat4_initial))
    obj_initial.reparentTo(base.render)

    # draw the object at the final object pose
    objpos_final = np.array([400,-300,1050])
    objrot_final = rm.rodrigues([0,0,1],-135)
    objmat4_final = rm.homobuild(objpos_final, objrot_final)
    obj_final = copy.deepcopy(obj)
    obj_final.setColor(0, 0, 1, .5)
    obj_final.setMat(base.pg.np4ToMat4(objmat4_final))
    obj_final.reparentTo(base.render)

    candidateidlist = [8]
    grasppair = []
    for candidateid in candidateidlist:
        prejawwidth, prehndfc, prehndmat4 = predefined_grasps[candidateid]
        hndmat4_initial = np.dot(objmat4_initial, prehndmat4)
        eepos_initial = rm.homotransform(objmat4_initial, prehndfc)[:3]
        eerot_initial = hndmat4_initial[:3,:3]
        hndmat4_final = np.dot(objmat4_final, prehndmat4)
        eepos_final = rm.homotransform(objmat4_final, prehndfc)[:3]
        eerot_final = hndmat4_final[:3,:3]
        grasppair.append([[eepos_initial, eerot_initial], [eepos_final, eerot_final]])

    eepos_initial, eerot_initial = grasppair[0][0]
    start = rbt.numik(eepos_initial, eerot_initial, armname)
    rbt.movearmfk(start, armname=armname)
    rbt.closegripper(armname, prejawwidth)
    rbtmg.genmnp(rbt,  togglejntscoord=False, rgbargt=[1.0, 0.0, 0.0, .5]).reparentTo(base.render)
    objrelpos, objrelrot = rbt.getinhandpose(objpos_initial, objrot_initial, armname)

    eepos_final, eerot_final = grasppair[0][1]
    goal = rbt.numik(eepos_final, eerot_final, armname)
    rbt.movearmfk(goal, armname=armname)
    rbt.opengripper(armname, prejawwidth)
    rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[0.0, 0.0, 1.0, .5]).reparentTo(base.render)

    # planning
    robotball = robotball.Ur3DualBall()
    cdchecker = cdck.CollisionCheckerBall(robotball)
    ctcallback = ctcb.CtCallback(base, rbt, cdchecker=cdchecker, ctchecker=None, armname=armname)

    pickupprimitive = ctcallback.getLinearPrimitive(start, [0,0,1], 70, [obj], [[objrelpos, objrelrot]], obscmlist, type="source")
    placedownprimitive = ctcallback.getLinearPrimitive(goal, [0,0,1], 70, [obj], [[objrelpos, objrelrot]], obscmlist, type="sink")

    rbt.movearmfk(pickupprimitive[-1], armname=armname)
    rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[1.0, 0.0, 1.0, .5]).reparentTo(base.render)
    objpos, objrot = rbt.getworldpose(objrelpos, objrelrot, armname)
    objmat4=rm.homobuild(objpos, objrot)
    obj= copy.deepcopy(obj)
    obj.setColor(1.0, 0.0, 1.0, .5)
    obj.setMat(base.pg.np4ToMat4(objmat4))
    rbt.movearmfk(placedownprimitive[0], armname=armname)
    rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[0.0, 1.0, 1.0, .5]).reparentTo(base.render)
    objpos, objrot = rbt.getworldpose(objrelpos, objrelrot, armname)
    objmat4=rm.homobuild(objpos, objrot)
    obj= copy.deepcopy(obj)
    obj.setColor(0.0, 1.0, 1.0, .5)
    obj.setMat(base.pg.np4ToMat4(objmat4))
    planner = rrtc.RRTConnect(start=pickupprimitive[-1], goal=placedownprimitive[0], ctcallback=ctcallback,
                                starttreesamplerate=30,
                                endtreesamplerate=30, expanddis=10,
                                maxiter=2000, maxtime=100.0)
    path, samples = planner.planninghold([obj], [[objrelpos, objrelrot]], obscmlist)
    # for i, armjnts in enumerate(path):
    #     rbt.movearmfk(armjnts, armname)
        # rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[1.0-i/len(path), 0.0, i/len(path), .2]).reparentTo(base.render)
        # rbtmg.genesnp(rbt, rgtrgba=[0.5,0.5,0,1]).reparentTo(base.render)

    smoother = sm.Smoother()
    path = smoother.pathsmoothinghold(path, planner)
    path = pickupprimitive+path+placedownprimitive
    # path = path
    for i, armjnts in enumerate(path):
        rbt.movearmfk(armjnts, armname)
        # rbtmg.genmnp(rbt, jawwidthrgt=prejawwidth, togglejntscoord=False, rgbargt=[1.0, 0.0, i/len(path), .5]).reparentTo(base.render)
        # rbtmg.genmnp(rbt, jawwidthrgt=prejawwidth, togglejntscoord=False, rgbargt=[0.0, 1.0-i/len(path), 1.0, .5]).reparentTo(base.render)
        # print([math.cos(i/len(path)*math.pi*2), i/len(path), 1.0-i/len(path), .5])
        # rbtmg.genmnp(rbt, jawwidthrgt=prejawwidth, togglejntscoord=False, rgbargt=[1.0-i/len(path), i/len(path), abs(math.cos(i/len(path)*math.pi)), .5]).reparentTo(base.render)
        rbtmg.genesnp(rbt, rgtrgba=[0,0.5,0,1]).reparentTo(base.render)
        # objpos, objrot = rbt.getworldpose(objrelpos, objrelrot, armname)
        # objmat4=rm.homobuild(objpos, objrot)
        # obj= copy.deepcopy(obj)
        # obj.setColor(1.0, 0.0, i/len(path), .5)
        # obj.setColor(0.0, 1.0-i/len(path), 1.0, .5)
        # obj.setColor(1.0-i/len(path), i/len(path), abs(math.cos(i/len(path)*math.pi)), .5)
        # obj.setMat(base.pg.np4ToMat4(objmat4))
        # obj.reparentTo(base.render)
    #
    #
    def update(rbtmnp, objmnp, motioncounter, rbt, path, armname, rbtmg, obj, objrelmat, task):
        if motioncounter[0] < len(path):
            if rbtmnp[0] is not None:
                rbtmnp[0].detachNode()
            if objmnp[0] is not None:
                objmnp[0].detachNode()
            pose = path[motioncounter[0]]
            rbt.movearmfk(pose, armname)
            rbtmnp[0] = rbtmg.genmnp(rbt)
            rbtmnp[0].reparentTo(base.render)
            objmnp[0] = copy.deepcopy(obj)
            objmnp[0].setColor(.8, .6, .3, .5)
            objpos, objrot = rbt.getworldpose(objrelmat[0], objrelmat[1], armname)
            objmat4=rm.homobuild(objpos, objrot)
            objmnp[0].setMat(base.pg.np4ToMat4(objmat4))
            objmnp[0].reparentTo(base.render)
            motioncounter[0] += 1
        else:
            motioncounter[0] = 0
        return task.again


    rbtmnp = [None]
    objmnp = [None]
    motioncounter = [0]
    taskMgr.doMethodLater(0.05, update, "update",
                          extraArgs=[rbtmnp, objmnp, motioncounter, rbt, path, armname, rbtmg, obj, [objrelpos, objrelrot]],
                          appendTask=True)

    base.run()