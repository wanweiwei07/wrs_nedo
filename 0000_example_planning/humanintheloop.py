"""
This file defines the obstacles

There are three types of objects in the obstacles:
1. Stationary: tables/aluminium frames
2. Changable: additional obstacles
3. Object

Stationary obstacles will be modeled as CollisionModel.
Only their bounding boxes are used for collision detection.
Changable and stationary objects are modeled as CollisionModelShare.
Either their bounding boxes or their ball approximations could be used for collision detection.
In most of the time, we use ball approximations.

author: weiwei
date: 20190313
"""

import os
import environment.collisionmodel as cm

class Env(object):

    def __init__(self, boundingradius = 2.0, betransparent=False):
        """
        load obstacles model
        separated by category

        :param base:
        author: weiwei
        date: 20181205
        """

        self.__this_dir, _ = os.path.split(__file__)

        # body
        self.__bodypath = os.path.join(self.__this_dir, "obstacles", "ur3body.stl")
        self.__bodycm = cm.CollisionModel(self.__bodypath, radius=boundingradius, betransparency=betransparent)
        self.__bodycm.setColor(.3, .3, .3, 1.0)

        # table
        self.__tablepath = os.path.join(self.__this_dir, "obstacles", "ur3dtablefree.stl")
        self.__tablecm = cm.CollisionModel(self.__tablepath, radius=boundingradius, betransparency=betransparent)
        self.__tablecm.setColor(.55, .55, .5, 1.0)
        self.__tablecm.setPos(50.0, 0.0, 0.0)

        # housing
        ## housing pillar
        self.__beam1200path = os.path.join(self.__this_dir, "obstacles", "ur3housing1200.stl")
        self.__beam1500path = os.path.join(self.__this_dir, "obstacles", "ur3housing1500.stl")
        self.__beam2100path = os.path.join(self.__this_dir, "obstacles", "ur3housing2100.stl")

        self.__pillarfrontrgtcm = cm.CollisionModel(self.__beam2100path, radius=boundingradius, betransparency=betransparent)
        self.__pillarfrontrgtcm.setColor(.5, .5, .55, 1.0)
        self.__pillarfrontrgtcm.setPos(860.0 + 50.0 + 30.0, -780.0, 0.0)
        self.__pillarfrontlftcm = cm.CollisionModel(self.__beam2100path, radius=boundingradius, betransparency=betransparent)
        self.__pillarfrontlftcm.setColor(.5, .5, .55, 1.0)
        self.__pillarfrontlftcm.setPos(860.0 + 50.0 + 30.0, 780.0, 0.0)
        self.__pillarbackrgtcm = cm.CollisionModel(self.__beam2100path, radius=boundingradius, betransparency=betransparent)
        self.__pillarbackrgtcm.setColor(.5, .5, .55, 1.0)
        self.__pillarbackrgtcm.setPos(860 + 50 - 1230, -780.0, 0.0)
        self.__pillarbacklftcm = cm.CollisionModel(self.__beam2100path, radius=boundingradius, betransparency=betransparent)
        self.__pillarbacklftcm.setColor(.5, .5, .55, 1.0)
        self.__pillarbacklftcm.setPos(860 + 50 - 1230, 780.0, 0.0)
        ## housing rgt
        self.__rowrgtbottomcm = cm.CollisionModel(self.__beam1200path, radius=boundingradius, betransparency=betransparent)
        self.__rowrgtbottomcm.setColor(.5, .5, .55, 1.0)
        self.__rowrgtbottomcm.setPos(860 + 50 - 600, -780.0, 75.0)
        self.__rowrgtmiddlecm = cm.CollisionModel(self.__beam1200path, radius=boundingradius, betransparency=betransparent)
        self.__rowrgtmiddlecm.setColor(.5, .5, .55, 1.0)
        self.__rowrgtmiddlecm.setPos(860 + 50 - 600, -780.0, 867.0)
        self.__rowrgttopcm = cm.CollisionModel(self.__beam1200path, radius=boundingradius, betransparency=betransparent)
        self.__rowrgttopcm.setColor(.5, .5, .55, 1.0)
        self.__rowrgttopcm.setPos(860 + 50 - 600, -780.0, 2100.0 - 60.0)
        ## housing lft
        self.__rowlftbottomcm = cm.CollisionModel(self.__beam1200path, radius=boundingradius, betransparency=betransparent)
        self.__rowlftbottomcm.setColor(.5, .5, .55, 1.0)
        self.__rowlftbottomcm.setPos(860 + 50 - 600, 780.0, 75.0)
        self.__rowlfttopcm = cm.CollisionModel(self.__beam1200path, radius=boundingradius, betransparency=betransparent)
        self.__rowlfttopcm.setColor(.5, .5, .55, 1.0)
        self.__rowlfttopcm.setPos(860 + 50 - 600, 780.0, 2100.0 - 60.0)
        ## housing front
        self.__rowfrontbottomcm = cm.CollisionModel(self.__beam1500path, radius=boundingradius, betransparency=betransparent)
        self.__rowfrontbottomcm.setColor(.5, .5, .55, 1.0)
        self.__rowfrontbottomcm.setPos(860 + 50 + 30, 0.0, 75.0)
        self.__rowfrontmiddlecm = cm.CollisionModel(self.__beam1500path, radius=boundingradius, betransparency=betransparent)
        self.__rowfrontmiddlecm.setColor(.5, .5, .55, 1.0)
        self.__rowfrontmiddlecm.setPos(860 + 50 + 30, 0.0, 867.0)
        self.__rowfronttopcm = cm.CollisionModel(self.__beam1500path, radius=boundingradius, betransparency=betransparent)
        self.__rowfronttopcm.setColor(.5, .5, .55,1.0)
        self.__rowfronttopcm.setPos(860+50+30, 0.0, 2100.0-60.0)
        ## housing back
        self.__rowbackbottomcm = cm.CollisionModel(self.__beam1500path, radius=boundingradius, betransparency=betransparent)
        self.__rowbackbottomcm.setColor(.5, .5, .55, 1.0)
        self.__rowbackbottomcm.setPos(860 + 50 - 1230, 0.0, 2100.0 - 530.0 - 60.0)
        self.__rowbackmiddlecm = cm.CollisionModel(self.__beam1500path, radius=boundingradius, betransparency=betransparent)
        self.__rowbackmiddlecm.setColor(.5, .5, .55, 1.0)
        self.__rowbackmiddlecm.setPos(860 + 50 - 1230, 0.0, 2100.0 - 264.0 - 60.0)
        self.__rowbacktopcm = cm.CollisionModel(self.__beam1500path, radius=boundingradius, betransparency=betransparent)
        self.__rowbacktopcm.setColor(.5, .5, .55, 1.0)
        self.__rowbacktopcm.setPos(860 + 50 - 1230, 0.0, 2100.0 - 60.0)

        self.__battached = False
        self.__changableobslist = []

    def reparentTo(self, nodepath):
        if not self.__battached:
            # body
            self.__bodycm.reparentTo(nodepath)
            # table
            self.__tablecm.reparentTo(nodepath)
            # housing
            ## housing pillar
            self.__pillarfrontrgtcm.reparentTo(nodepath)
            self.__pillarfrontlftcm.reparentTo(nodepath)
            self.__pillarbackrgtcm.reparentTo(nodepath)
            self.__pillarbacklftcm.reparentTo(nodepath)
            self.__rowrgtbottomcm.reparentTo(nodepath)
            self.__rowrgtmiddlecm.reparentTo(nodepath)
            self.__rowrgttopcm.reparentTo(nodepath)
            self.__rowlftbottomcm.reparentTo(nodepath)
            self.__rowlfttopcm.reparentTo(nodepath)
            self.__rowfrontbottomcm.reparentTo(nodepath)
            self.__rowfrontmiddlecm.reparentTo(nodepath)
            self.__rowfronttopcm.reparentTo(nodepath)
            self.__rowbackbottomcm.reparentTo(nodepath)
            self.__rowbackmiddlecm.reparentTo(nodepath)
            self.__rowbacktopcm.reparentTo(nodepath)
            self.__battached = True

    def loadobj(self, name):
        self.__objpath = os.path.join(self.__this_dir, "objects", name)
        self.__objcm = cm.CollisionModel(self.__objpath, type="ball")
        return self.__objcm

    def getstationaryobslist(self):
        """
        generate the collision model for stationary obstacles

        :return:

        author: weiwei
        date: 20180811
        """

        stationaryobslist = [self.__tablecm, self.__pillarfrontrgtcm, self.__pillarfrontlftcm,
                             self.__pillarbackrgtcm, self.__pillarbacklftcm,
                             self.__rowrgtbottomcm, self.__rowrgtmiddlecm, self.__rowrgttopcm,
                             self.__rowlftbottomcm, self.__rowlfttopcm,
                             self.__rowfrontbottomcm, self.__rowfrontmiddlecm,  self.__rowfronttopcm,
                             self.__rowbackbottomcm, self.__rowbackmiddlecm, self.__rowbacktopcm]
        return stationaryobslist

    def getchangableobslist(self):
        """
        get the collision model for changable obstacles

        :return:

        author: weiwei
        date: 20190313
        """
        return self.__changableobslist

    def addchangableobs(self, nodepath, objcm, pos, rot):
        """

        :param objcm: CollisionModel
        :param pos: nparray 1x3
        :param rot: nparray 3x3
        :return:

        author: weiwei
        date: 20190313
        """

        self.__changableobslist.append(objcm)
        objcm.reparentTo(nodepath)
        objcm.setMat(base.pg.npToMat4(rot, pos))

    def removechangableobs(self, objcm):
        if objcm in self.__changableobslist:
            objcm.remove()


if __name__ == '__main__':
    import utiltools.robotmath as rm
    import numpy as np
    import pandaplotutils.pandactrl as pandactrl
    import robotsim.ur3dual.ur3dual as robot
    import robotsim.ur3dual.ur3dualmesh as robotmesh
    import robotsim.nextage.nxt as human
    import robotsim.nextage.nxtmesh as humanmesh
    import robotsim.nextage.nxtik as humanik
    import manipulation.grip.robotiq85.robotiq85 as rtq85
    import manipulation.grip.humanhand.hhdw as hhdw
    from panda3d.core import NodePath
    import copy
    import trimesh

    base = pandactrl.World(camp=[2700, 2300, 2700], lookatp=[800, 0, 1000])

    env = Env(boundingradius=7.0)
    env.reparentTo(base.render)

    obscmlist = env.getstationaryobslist()
    for obscm in obscmlist:
        obscm.showcn()

    # objcm = env.loadobj("bunnysim.stl")
    # objcm.setColor(.2, .5, 0, 1)
    # objcm.setPos(400, -200, 1200)
    # objcm.reparentTo(base.render)
    # objcm.showcn()
    #
    # objpos = np.array([400, -300, 1200])
    # objrot = rm.rodrigues([0, 1, 0], 45)
    # objcm2 = env.loadobj("housing.stl")
    # objcm2.setColor(1, .5, 0, 1)
    # env.addchangableobs(base.render, objcm2, objpos, objrot)
    #
    # objcm3 = cm.CollisionModel(trimesh.primitives.Box(box_center=[500,100,1500], box_extents=[200,50,100]))
    # objcm3.setColor(1,0,0,1)
    # objcm3.reparentTo(base.render)
    # objcm3.showcn()

    hndfa = rtq85.Robotiq85Factory()
    rgthnd = hndfa.genHand()
    lfthnd = hndfa.genHand()
    rbt = robot.Ur3DualRobot(rgthnd=rgthnd, lfthnd=lfthnd)
    rbtmg = robotmesh.Ur3DualMesh()
    rbtmnp = rbtmg.genmnp(rbt, toggleendcoord=True)
    rbtmnp.reparentTo(base.render)

    hhdfa = hhdw.HHdwFactory()
    rgthmhnd = hhdfa.genHand(ftsensoroffset=0, armname="rgt")
    lfthmhnd = hhdfa.genHand(ftsensoroffset=0, armname="lft")
    hmn = human.NxtRobot(rgthnd=rgthmhnd, lfthnd=lfthmhnd, position=[1300, 0, 200], rotmat=rm.rodrigues([0,0,1], 180))
    hmnmg = humanmesh.NxtMesh()
    hmnmnp = hmnmg.gensnp(hmn, toggleendcoord=True, drawhand=True)
    hmnmnp.reparentTo(base.render)

    __this_dir, _ = os.path.split(__file__)
    cabinet = NodePath("cabinet")

    premiddle = []

    #0
    premiddle.append(rgthmhnd.approachAt(73,150,5,0,0,1,0,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,150,5,0,0,1,0,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,150,5,0,0,1,0,-1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,150,5,0,0,-1,0,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,150,5,0,0,-1,0,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,150,5,0,0,-1,0,-1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,-150,5,0,0,1,0,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,-150,5,0,0,1,0,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,-150,5,0,0,1,0,1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,-150,5,0,0,-1,0,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,-150,5,0,0,-1,0,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,-150,5,0,0,-1,0,1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(110,100,5,0,0,1,-1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,0,5,0,0,1,-1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,-100,5,0,0,1,-1,0,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(110,100,5,0,0,-1,-1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,0,5,0,0,-1,-1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,-100,5,0,0,-1,-1,0,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(-110,100,5,0,0,1,1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,0,5,0,0,1,1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,-100,5,0,0,1,1,0,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(-110,100,5,0,0,-1,1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,0,5,0,0,-1,1,0,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,-100,5,0,0,-1,1,0,0,jawwidth=60))

    # +30
    premiddle.append(rgthmhnd.approachAt(73,150,5,0,0,1,.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,150,5,0,0,1,.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,150,5,0,0,1,.5,-1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,150,5,0,0,-1,.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,150,5,0,0,-1,.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,150,5,0,0,-1,.5,-1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,-150,5,0,0,1,.5,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,-150,5,0,0,1,.5,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,-150,5,0,0,1,.5,1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,-150,5,0,0,-1,.5,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,-150,5,0,0,-1,1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,-150,5,0,0,-1,.5,1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(110,100,5,0,0,1,-1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,0,5,0,0,1,-1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,-100,5,0,0,1,-1,.5,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(110,100,5,0,0,-1,-1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,0,5,0,0,-1,-1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,-100,5,0,0,-1,-1,.5,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(-110,100,5,0,0,1,1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,0,5,0,0,1,1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,-100,5,0,0,1,1,.5,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(-110,100,5,0,0,-1,1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,0,5,0,0,-1,1,.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,-100,5,0,0,-1,1,.5,0,jawwidth=60))

    # -30
    premiddle.append(rgthmhnd.approachAt(73,150,5,0,0,1,-.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,150,5,0,0,1,-.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,150,5,0,0,1,-.5,-1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,150,5,0,0,-1,-.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,150,5,0,0,-1,-.5,-1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,150,5,0,0,-1,-.5,-1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,-150,5,0,0,1,-.5,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,-150,5,0,0,1,-.5,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,-150,5,0,0,1,-.5,1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(73,-150,5,0,0,-1,-.5,1,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(0,-150,5,0,0,-1,1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-73,-150,5,0,0,-1,-.5,1,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(110,100,5,0,0,1,-1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,0,5,0,0,1,-1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,-100,5,0,0,1,-1,-.5,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(110,100,5,0,0,-1,-1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,0,5,0,0,-1,-1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(110,-100,5,0,0,-1,-1,-.5,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(-110,100,5,0,0,1,1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,0,5,0,0,1,1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,-100,5,0,0,1,1,-.5,0,jawwidth=60))

    premiddle.append(rgthmhnd.approachAt(-110,100,5,0,0,-1,1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,0,5,0,0,-1,1,-.5,0,jawwidth=60))
    premiddle.append(rgthmhnd.approachAt(-110,-100,5,0,0,-1,1,-.5,0,jawwidth=60))

    prelarge = []

    #0
    prelarge.append(rgthmhnd.approachAt(73,255,5,0,0,1,0,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,255,5,0,0,1,0,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,255,5,0,0,1,0,-1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,255,5,0,0,-1,0,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,255,5,0,0,-1,0,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,255,5,0,0,-1,0,-1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,-255,5,0,0,1,0,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,-255,5,0,0,1,0,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,-255,5,0,0,1,0,1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,-255,5,0,0,-1,0,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,-255,5,0,0,-1,0,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,-255,5,0,0,-1,0,1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(110,145,5,0,0,1,-1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,0,5,0,0,1,-1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,-145,5,0,0,1,-1,0,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(110,145,5,0,0,-1,-1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,0,5,0,0,-1,-1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,-145,5,0,0,-1,-1,0,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(-110,145,5,0,0,1,1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,0,5,0,0,1,1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,-145,5,0,0,1,1,0,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(-110,145,5,0,0,-1,1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,0,5,0,0,-1,1,0,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,-145,5,0,0,-1,1,0,0,jawwidth=60))

    # +30
    prelarge.append(rgthmhnd.approachAt(73,255,5,0,0,1,.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,255,5,0,0,1,.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,255,5,0,0,1,.5,-1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,255,5,0,0,-1,.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,255,5,0,0,-1,.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,255,5,0,0,-1,.5,-1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,-255,5,0,0,1,.5,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,-255,5,0,0,1,.5,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,-255,5,0,0,1,.5,1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,-255,5,0,0,-1,.5,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,-255,5,0,0,-1,1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,-255,5,0,0,-1,.5,1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(110,145,5,0,0,1,-1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,0,5,0,0,1,-1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,-145,5,0,0,1,-1,.5,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(110,145,5,0,0,-1,-1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,0,5,0,0,-1,-1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,-145,5,0,0,-1,-1,.5,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(-110,145,5,0,0,1,1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,0,5,0,0,1,1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,-145,5,0,0,1,1,.5,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(-110,145,5,0,0,-1,1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,0,5,0,0,-1,1,.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,-145,5,0,0,-1,1,.5,0,jawwidth=60))

    # -30
    prelarge.append(rgthmhnd.approachAt(73,255,5,0,0,1,-.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,255,5,0,0,1,-.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,255,5,0,0,1,-.5,-1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,255,5,0,0,-1,-.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,255,5,0,0,-1,-.5,-1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,255,5,0,0,-1,-.5,-1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,-255,5,0,0,1,-.5,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,-255,5,0,0,1,-.5,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,-255,5,0,0,1,-.5,1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(73,-255,5,0,0,-1,-.5,1,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(0,-255,5,0,0,-1,1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-73,-255,5,0,0,-1,-.5,1,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(110,145,5,0,0,1,-1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,0,5,0,0,1,-1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,-145,5,0,0,1,-1,-.5,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(110,145,5,0,0,-1,-1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,0,5,0,0,-1,-1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(110,-145,5,0,0,-1,-1,-.5,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(-110,145,5,0,0,1,1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,0,5,0,0,1,1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,-145,5,0,0,1,1,-.5,0,jawwidth=60))

    prelarge.append(rgthmhnd.approachAt(-110,145,5,0,0,-1,1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,0,5,0,0,-1,1,-.5,0,jawwidth=60))
    prelarge.append(rgthmhnd.approachAt(-110,-145,5,0,0,-1,1,-.5,0,jawwidth=60))
    
    middleboardpath = os.path.join(__this_dir, "objects", "middleboard.stl")
    middleboard0 = cm.CollisionModel(middleboardpath, radius=3, betransparency=True)
    middleboard1 = copy.deepcopy(middleboard0)
    middleboard1.setPos(0,0,288.5)
    middleboard2 = copy.deepcopy(middleboard0)
    middleboard2.setPos(0,0,577)

    middleboard0.setColor(.4,.4,.4,.3)
    middleboard0.reparentTo(cabinet)
    middleboard1.setColor(.4,.4,.4,.3)
    middleboard1.reparentTo(cabinet)
    middleboard2.setColor(.4,.4,.4,.3)
    middleboard2.reparentTo(cabinet)

    largeboardpath = os.path.join(__this_dir, "objects", "largeboard.stl")
    largeboard0 = cm.CollisionModel(largeboardpath, radius=3, betransparency=True)
    largeboard1 = copy.deepcopy(largeboard0)
    largeboard0.sethomomat(rm.homobuild(np.array([0,195,293.5]), np.array(rm.rodrigues([1,0,0], -90))))
    largeboard1.sethomomat(rm.homobuild(np.array([0,-195,293.5]), np.array(rm.rodrigues([1,0,0], 90))))

    largeboard0.setColor(.4,.4,.4,.3)
    largeboard0.reparentTo(cabinet)
    largeboard1.setColor(.4,.4,.4,.3)
    largeboard1.reparentTo(cabinet)
    
    smallboardpath = os.path.join(__this_dir, "objects", "smallboard.stl")
    smallboard0 = cm.CollisionModel(smallboardpath, radius=3, betransparency=True)
    smallboard1 = copy.deepcopy(smallboard0)
    smallboard0.sethomomat(rm.homobuild(np.array([-142.5,0,149.25]), np.array(rm.rodrigues([0,1,0], -90))))
    smallboard1.sethomomat(rm.homobuild(np.array([-142.5,0,587-149.25]), np.array(rm.rodrigues([0,1,0], -90))))

    smallboard0.setColor(.4,.4,.4,.3)
    smallboard0.reparentTo(cabinet)
    smallboard1.setColor(.4,.4,.4,.3)
    smallboard1.reparentTo(cabinet)

    cabinet.setMat(base.pg.np4ToMat4(rm.homobuild(np.array([780,-20,1030]),rm.rodrigues([0,0,1],15))))
    cabinet.reparentTo(base.render)

    smallboardtemp = copy.deepcopy(smallboard0)
    lbmat4 = smallboardtemp.gethomomat4()
    lbmat4[:3,3]=lbmat4[:3,3]+50*lbmat4[:3,0]+50*-lbmat4[:3,1]+30*lbmat4[:3,2]
    lbmat4[:3,:3]=np.dot(rm.rodrigues([0,1,0], 30), np.dot(rm.rodrigues([0,0,1], 10), lbmat4[:3,:3]))
    smallboardtemp.sethomomat(lbmat4)
    smallboardtemp.setColor(.4,.4,.4,1)
    smallboardtemp.reparentTo(cabinet)

    import matplotlib.pyplot as plt
    cmap = plt.get_cmap('rainbow')

    lbmat4 = smallboardtemp.gethomomat4()
    for gc in prelarge:
        eerot = np.dot(lbmat4[:3,:3], gc[2][:3,:3])
        eepos = np.dot(lbmat4[:3,:3], gc[1])+lbmat4[:3,3]+base.pg.v3ToNp(cabinet.getPos())
        boardcom = lbmat4[:3,3]+base.pg.v3ToNp(cabinet.getPos())
        if np.linalg.norm(boardcom-eepos) < 200:
            rgtjnts = hmn.numik(eepos, eerot, armname='lft')
            print(rgtjnts)
            if rgtjnts is not None:
                hmn.movearmfk(rgtjnts, armname='lft')
                ic = humanik.manipulability_inversecondition(hmn, armname='lft')*700
                rgb = cmap(ic)
                hmnmnp = hmnmg.gensnp(hmn, drawhand=True, rgbalfthnd=np.array([rgb[0],rgb[1],rgb[2],.5]))
                hmnmnp.reparentTo(base.render)

    # for gc in prelarge:
    #     hnd = rgthmhnd.copy()
    #     hnd.setMat(base.pg.np4ToMat4(gc[2]))
    #     hnd.reparentTo(smallboardtemp.objnp)


    # largeboardtemp = copy.deepcopy(largeboard0)
    # lbmat4 = largeboardtemp.gethomomat4()
    # lbmat4[:3,3]=lbmat4[:3,3]+20*lbmat4[:3,0]+50*-lbmat4[:3,1]+50*lbmat4[:3,2]
    # lbmat4[:3,:3]=np.dot(rm.rodrigues([0,1,0], -15), np.dot(rm.rodrigues([0,0,1], 10), lbmat4[:3,:3]))
    # largeboardtemp.sethomomat(lbmat4)
    # largeboardtemp.setColor(.4,.4,.4,1)
    # largeboardtemp.reparentTo(cabinet)
    # #
    #
    # import matplotlib.pyplot as plt
    # cmap = plt.get_cmap('rainbow')

    # lbmat4 = largeboardtemp.gethomomat4()
    # for gc in prelarge:
    #     eerot = np.dot(lbmat4[:3,:3], gc[2][:3,:3])
    #     eepos = np.dot(lbmat4[:3,:3], gc[1])+lbmat4[:3,3]+base.pg.v3ToNp(cabinet.getPos())
    #     boardcom = lbmat4[:3,3]+base.pg.v3ToNp(cabinet.getPos())
    #     if np.linalg.norm(boardcom-eepos) < 200:
    #         rgtjnts = hmn.numik(eepos, eerot, armname='rgt')
    #         print(rgtjnts)
    #         if rgtjnts is not None:
    #             hmn.movearmfk(rgtjnts, armname='rgt')
    #             ic = humanik.manipulability_inversecondition(hmn, armname='rgt')*700
    #             rgb = cmap(ic)
    #             hmnmnp = hmnmg.gensnp(hmn, drawhand=True, rgbargthnd=np.array([rgb[0],rgb[1],rgb[2],.5]))
    #             hmnmnp.reparentTo(base.render)


    # for gc in prelarge:
    #     hnd = rgthmhnd.copy()
    #     hnd.setMat(base.pg.np4ToMat4(gc[2]))
    #     hnd.reparentTo(largeboardtemp.objnp)


    base.run()
