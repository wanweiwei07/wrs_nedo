import os
import database.dbaccess as db
import pandaplotutils.pandactrl as pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import manipulation.grip.freegrip as freegrip
import environment.collisionmodel as cm

if __name__=='__main__':

    base = pandactrl.World(camp=[700,300,700], lookatp=[0,0,100])
    this_dir, this_filename = os.path.split(__file__)
    print(this_dir)
    # objpath = os.path.join(this_dir, "objects", "housing.stl")
    # objpath = os.path.join(this_dir, "objects", "box.stl")
    # objpath = os.path.join(this_dir, "objects", "tool_motordriver.stl")
    # objpath = os.path.join(this_dir, "objects", "housingshaft.stl")
    objpath = os.path.join(this_dir, "objects", "bunnysim.stl")

    hndfa = rtq85.Robotiq85Factory()
    hnd = hndfa.genHand()

    fgplanner = freegrip.Freegrip(objpath, hnd)
    objcm = cm.CollisionModel(objinit = objpath)
    objcm.reparentTo(base.render)
    objcm.setColor(.3,.3,.3,1)

    print("Planning...")
    fgplanner.planGrasps()
    gdb = db.GraspDB(database="nxt")
    print("Saving to database...")
    fgplanner.saveToDB(gdb)

    print("Loading from database...")
    fgdata = freegrip.FreegripDB(gdb, objcm.name, hnd.name)
    print("Plotting...")
    fgdata.plotHands(base.render, hndfa, rgba=(0,1,0,.3))
    base.run()