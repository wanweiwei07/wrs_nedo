import pandaplotutils.pandactrl as pc
import utiltools.robotmath as rm
import numpy as np
import environment.collisionmodel as cm
import trimesh as tm
import math
from panda3d.core import Mat4

if __name__=="__main__":
    base = pc.World(camp = [3000,0,5000], lookatp = [0,0,700])

    npidmat4 = np.eye(4)
    npidmat4[:3,3]=np.array([300,-700,1200])
    pdidmat4 = base.pg.np4ToMat4(npidmat4)
    print(pdidmat4.getRow3(3))
    # base.pggen.plotAxis(base.render, spos=pdidmat4.getRow3(3), pandamat3=pdidmat4.getUpper3())

    npidmat4[:3,:3] = rm.rodrigues([1,0,0],-180)
    pdmat4 = base.pg.np4ToMat4(npidmat4)
    base.pggen.plotAxis(base.render, spos=pdmat4.getRow3(3), pandamat3=pdmat4.getUpper3(), alpha=.3)
    base.pggen.plotCircArrow(base.render, axis=[-1, 0, 0], torqueportion=0.9, center=[150+300, -700, +1200], radius=70, thickness=7, rgba=[0.5, 0.5, 0.5, 1])
    base.run()