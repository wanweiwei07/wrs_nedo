import grpc
import time
import yaml
from concurrent import futures
import pblgrpc.cam_pb2 as cammsg
import pblgrpc.cam_pb2_grpc as camrpc
import cv2
import numpy as np

class CamServer(camrpc.CamServicer):
    cap = cv2.VideoCapture(0)

    def getimgstr(self, request, context):
        """

        Inherited from the auto-generated handcam_pb2_grpc

        :param request:
        :param context:
        :return:

        author: weiwei
        date: 20190416
        """

        ret, frame = self.cap.read()
        return cammsg.CamImgStr(data=yaml.dump(frame))

    def getimgbytes(self, request, context):
        """

        Inherited from the auto-generated handcam_pb2_grpc

        :param request:
        :param context:
        :return:

        author: weiwei
        date: 20190416
        """

        ret, frame = self.cap.read()
        imgh, imgw, nch = frame.shape
        fmbytes = np.ndarray.tobytes(frame)
        return cammsg.CamImgBytes(width=imgw, height=imgh, channel=nch, image=fmbytes)

def serve():
    _ONE_DAY_IN_SECONDS = 60 * 60 * 24

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    camrpc.add_CamServicer_to_server(CamServer(), server)
    server.add_insecure_port('[::]:18300')
    server.start()
    print("The Cam server is started!")
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()